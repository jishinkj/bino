import pandas as pd
import numpy as np

train_df = pd.read_csv("data/ames_train.csv", usecols = ['MSSubClass', 'MSZoning', 'LotFrontage', 'SalePrice'])
test_df = pd.read_csv("data/ames_test.csv", usecols = ['Id', 'MSSubClass', 'MSZoning', 'LotFrontage'])

len_train = len(train_df)
test_ID = test_df['Id']
#%%

# concatenate train and test sets
y_true = train_df['SalePrice']

train_df.drop(columns = ['SalePrice'], axis = 1, inplace = True)
test_df.drop(columns = ['Id'], axis = 1, inplace = True)


df = pd.concat([train_df, test_df], axis = 0)

#%% Missing Values

df.isnull().sum()

100*df.isnull().sum()/len(df)

df['MSZoning'].unique()
100*df['MSZoning'].value_counts()/len(df)


df['MSZoning'] = df['MSZoning'].fillna("RL")

df['MSZoning'] = df['MSZoning'].fillna(df['MSZoning'].mode()[0])

df['LotFrontage'].mean()
df['LotFrontage'].median()

df['LotFrontage'] = df['LotFrontage'].fillna(df['LotFrontage'].mean())

#%%

# Handle categorical variables - Dummy encoding

df['MSSubClass'] = df['MSSubClass'].astype(str)

df['MSSubClass'].nunique()   # 16
df['MSZoning'].nunique()     # 5

#cat_columns = ['MSSubClass', 'MSZoning']

cat_columns = []
for column in df.columns:
    if df[column].dtype == 'object':
        cat_columns.append(column)

#cat_columns = [column for column in df.columns if df[column].dtype == 'object'] # list comprehension
encoded_df = pd.get_dummies(df, columns = cat_columns, drop_first = True)

#%% Separate train and submission sets

train_df = encoded_df[:len(train_df)]
submission_df = encoded_df[len_train:]

#%% train test split
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(train_df, y_true, test_size = 0.2, random_state = 42)

#%% Modeling 
from sklearn.linear_model import LinearRegression

reg_obj = LinearRegression() # creating an instance of this class, created instance is called an object
reg_obj.fit(X_train, y_train)

y_pred = reg_obj.predict(X_test)

#%% Evaluation Metrics

#evaluation_df = pd.DataFrame({'y_test' : y_test, 'y_pred': y_pred})
#evaluation_df['error'] = evaluation_df['y_pred'] - evaluation_df['y_test']
#evaluation_df['err_squared'] = evaluation_df['error']*evaluation_df['error']
#
#SSE = evaluation_df['err_squared'].sum()
#MSE = evaluation_df['err_squared'].sum()/len(evaluation_df)

#%% Prediction on submission dataset
submission_predictions = reg_obj.predict(submission_df)

print("\n==================Step 7: Predict and Export to CSV ===================")
submission = np.clip(a = submission_predictions, a_min = 0.0, a_max = 500000.0)

#%% Export to CSV

kaggle_df = pd.DataFrame({'Id': test_ID, 'SalePrice': submission})

kaggle_df.to_csv("data/v1.csv", index = False, header = True)

